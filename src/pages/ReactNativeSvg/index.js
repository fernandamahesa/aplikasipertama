import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import IlustrationMyApp from '../../assets/icons/ilustration-my-app.svg';

const ReactNativeSvg = () => {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.title}>
        Materi Menggunakan File Svg di React Native
      </Text>
      <IlustrationMyApp width={244} height={125} />
    </View>
  );
};

export default ReactNativeSvg;

const styles = StyleSheet.create({
  wrapper: {
    padding: 20,
  },
  title: {
    textAlign: 'center',
    marginBottom: 20,
  },
});
