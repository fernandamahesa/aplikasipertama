import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
  Image,
  ScrollView,
  TouchableOpacity,
  Alert,
} from 'react-native';

const Item = ({name, email, bidang, onPress, onDelete}) => {
  return (
    <View style={styles.containerItem}>
      <TouchableOpacity onPress={onPress}>
        <Image
          source={{uri: 'https://placeimg.com/160/160/people'}}
          style={styles.avatar}
        />
      </TouchableOpacity>
      <View style={{marginLeft: 12, justifyContent: 'center', flex: 1}}>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.email}>{email}</Text>
        <Text style={styles.bidang}>{bidang}</Text>
      </View>
      <TouchableOpacity onPress={onDelete}>
        <Text style={styles.delete}>X</Text>
      </TouchableOpacity>
    </View>
  );
};

const LocalAPI = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [bidang, setBidang] = useState('');
  const [users, setUsers] = useState([]);
  const [button, setButton] = useState('Simpan');
  const [selectedUser, setSelectedUser] = useState({});

  useEffect(() => {
    getData();
  }, []);

  const submit = () => {
    const data = {
      name,
      email,
      bidang,
    };
    // console.log('data before send', data);
    if (button === 'SIMPAN') {
      axios
        .post('http://10.0.2.2:3004/users', data)
        .then((res) => {
          //   console.log('success', res);
          setName('');
          setEmail('');
          setBidang('');
          getData();
        })
        .catch((err) => {
          console.log('error', err);
        });
    } else if (button === 'UPDATE') {
      axios
        .put(`http://10.0.2.2:3004/users/${selectedUser.id}`, data)
        .then((res) => {
          //   console.log('res update', res);
          setName('');
          setEmail('');
          setBidang('');
          getData();
          setButton('SIMPAN');
        })
        .catch((err) => {
          console.log('error', err);
        });
    }
  };

  const getData = () => {
    axios
      .get('http://10.0.2.2:3004/users')
      .then((res) => {
        // console.log('getData', res);
        setUsers(res.data);
      })
      .catch((err) => {
        console.log('error', err);
      });
  };

  const selectItem = (item) => {
    // console.log('item', item);
    setSelectedUser(item);
    setName(item.name);
    setEmail(item.email);
    setBidang(item.bidang);
    setButton('UPDATE');
  };

  const deleteItem = (item) => {
    console.log('item', item);
    axios
      .delete(`http://10.0.2.2:3004/users/${item.id}`)
      .then((res) => {
        console.log('res delete', res);
        getData();
      })
      .catch((err) => {
        console.log('error', err);
      });
  };

  return (
    <ScrollView>
      <View style={styles.container}>
        <Text style={styles.title}>Local API (JSON Server)</Text>
        <Text style={styles.titleWeb}>Daftar Web Developer</Text>
        <TextInput
          placeholder="Nama Lengkap"
          style={styles.textInput}
          value={name}
          onChangeText={(value) => {
            setName(value);
          }}
        />
        <TextInput
          placeholder="Email"
          style={styles.textInput}
          value={email}
          onChangeText={(value) => {
            setEmail(value);
          }}
        />
        <TextInput
          placeholder="Bidang"
          style={styles.textInput}
          value={bidang}
          onChangeText={(value) => {
            setBidang(value);
          }}
        />
        <Button title={button} onPress={submit} />
        <View style={styles.line} />
        {users.map((user) => {
          return (
            <Item
              key={user.id}
              name={user.name}
              email={user.email}
              bidang={user.bidang}
              onPress={() => selectItem(user)}
              onDelete={() =>
                Alert.alert('Peringatan', 'Anda yakin akan menghapus ini ?', [
                  {
                    text: 'Tidak',
                    onPress: () => console.log('button tidak'),
                  },
                  {
                    text: 'Ya',
                    onPress: () => deleteItem(user),
                  },
                ])
              }
            />
          );
        })}
      </View>
    </ScrollView>
  );
};

export default LocalAPI;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  title: {
    textAlign: 'center',
    marginBottom: 20,
  },
  titleWeb: {
    fontWeight: 'bold',
    marginBottom: 12,
  },
  textInput: {
    borderWidth: 1,
    borderRadius: 14,
    borderColor: '#1e212d',
    marginBottom: 12,
    marginTop: 10,
  },
  line: {
    height: 2,
    backgroundColor: '#1e212d',
    marginVertical: 20,
  },
  avatar: {
    height: 80,
    width: 80,
    borderRadius: 80,
  },
  containerItem: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  name: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  email: {
    fontSize: 12,
  },
  bidang: {
    marginTop: 6,
    fontSize: 12,
  },
  delete: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#e40017',
  },
});
