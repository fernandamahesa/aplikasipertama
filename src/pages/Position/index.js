import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import Keranjang from '../../assets/icons/keranjang.png';

const Position = () => {
  return (
    <View style={styles.wrapper}>
      <Text>Materi Positon RN</Text>
      <View style={styles.cartWrapper}>
        <Image source={Keranjang} style={styles.iconCart} />
        <Text style={styles.notifCart}>10</Text>
      </View>
      <Text style={styles.text}>Keranjang Belanja Anda</Text>
    </View>
  );
};

export default Position;

const styles = StyleSheet.create({
  wrapper: {padding: 20, alignItems: 'center'},
  cartWrapper: {
    borderWidth: 1,
    borderColor: '#333333',
    width: 93,
    height: 93,
    borderRadius: 93 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    position: 'relative',
  },
  iconCart: {
    width: 50,
    height: 50,
  },
  notifCart: {
    backgroundColor: '#4398d1',
    padding: 5,
    position: 'absolute',
    borderRadius: 50,
    top: 0,
    right: 0,
  },
  text: {
    fontSize: 14,
    fontWeight: '500',
    marginTop: 14,
  },
});
