import React, {Component} from 'react';
import {View, Text, Image, TextInput} from 'react-native';

const SampleComponent = () => {
  return (
    <View>
      <View style={{height: 100, width: 100, backgroundColor: '#3b5998'}} />
      <Text>Fernada Mahesa</Text>
      <Job />
      <Text>Karinia Sumitha Hassan</Text>
      <TextInput style={{borderWidth: 1}} />
      <Photo />
      <Profile />
    </View>
  );
};

const Job = () => {
  return <Text>Web Developer</Text>;
};

const Photo = () => {
  return (
    <Image
      source={{uri: 'https://placeimg.com/100/100/tech'}}
      style={{height: 100, width: 100}}
    />
  );
};

class Profile extends Component {
  render() {
    return (
      <View>
        <Text style={{fontSize: 20, color: '#ff6f69'}}>
          Ini component dari class
        </Text>
        <Image
          source={{uri: 'https://placeimg.com/100/100/animals'}}
          style={{width: 100, height: 100, borderRadius: 50}}
        />
      </View>
    );
  }
}

export default SampleComponent;
