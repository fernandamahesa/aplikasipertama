import React from 'react';
import {View, Text, Image, ScrollView} from 'react-native';

const Stroy = (props) => {
  return (
    <View style={{marginRight: 14, alignItems: 'center', marginTop: 10}}>
      <Image
        source={{uri: props.image}}
        style={{height: 50, width: 50, borderRadius: 50 / 2}}
      />
      <Text>{props.title}</Text>
    </View>
  );
};

const PropsDinamis = () => {
  return (
    <View>
      <Text style={{textAlign: 'center'}}>
        Materi Component Dinamis dengan Props
      </Text>
      <ScrollView horizontal>
        <View style={{flexDirection: 'row'}}>
          <Stroy image="https://placeimg.com/50/50/animals" title="Animals" />
          <Stroy image="https://placeimg.com/50/50/arch" title="Arch" />
          <Stroy image="https://placeimg.com/50/50/nature" title="Nature" />
          <Stroy image="https://placeimg.com/50/50/people" title="People" />
          <Stroy image="https://placeimg.com/50/50/tech" title="Tech" />
          <Stroy image="https://placeimg.com/50/50/animals" title="Animals" />
          <Stroy image="https://placeimg.com/50/50/arch" title="Arch" />
        </View>
      </ScrollView>
    </View>
  );
};

export default PropsDinamis;
