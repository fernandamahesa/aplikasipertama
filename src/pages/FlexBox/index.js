import React, {Component} from 'react';
import {Image, Text, View} from 'react-native';
import PhotoProfile from '../../assets/images/fernandamahesa.jpeg';

class FlexBox extends Component {
  render() {
    console.log('hello debugger');
    return (
      <View>
        <View
          style={{
            backgroundColor: '#c8d6e5',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View style={{backgroundColor: '#ee5253', width: 50, height: 50}} />
          <View style={{backgroundColor: '#1dd1a1', width: 50, height: 50}} />
          <View style={{backgroundColor: '#feca57', width: 50, height: 50}} />
          <View style={{backgroundColor: '#5f27cd', width: 50, height: 50}} />
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 20,
          }}>
          <Image
            source={PhotoProfile}
            style={{width: 100, height: 100, borderRadius: 50}}
          />
          <View style={{marginLeft: 14}}>
            <Text style={{fontSize: 20, fontWeight: 'bold'}}>
              Fernanda Mahesa
            </Text>
            <Text>Web Developer</Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            marginTop: 20,
          }}>
          <Text>Beranda</Text>
          <Text>Video</Text>
          <Text>Playlist</Text>
          <Text>Komunitas</Text>
          <Text>Channel</Text>
          <Text>Tentang</Text>
        </View>
      </View>
    );
  }
}

export default FlexBox;
