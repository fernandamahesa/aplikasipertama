import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Button, Image, StyleSheet, Text, View} from 'react-native';

const CallAPIAxios = () => {
  const [dataUser, setDataUser] = useState({
    avatar: '',
    email: '',
    first_name: '',
    last_name: '',
  });

  const [dataJob, setDataJob] = useState({
    name: '',
    job: '',
  });

  const getData = () => {
    axios
      .get('https://reqres.in/api/users/3')
      .then((res) => {
        // console.log('success', res);
        setDataUser(res.data.data);
      })
      .catch((err) => {
        console.log('error', err);
      });
  };

  const postData = () => {
    const dataForAPI = {
      name: 'morpheus',
      job: 'leader',
    };

    axios
      .post('https://reqres.in/api/users', dataForAPI)
      .then((res) => {
        // console.log('success', res);
        setDataJob(res.data);
      })
      .catch((err) => {
        console.log('error', err);
      });
  };

  return (
    <View style={styles.wrapper}>
      <Text style={styles.title}>Call API Menggunakan Axios</Text>
      <Button title="GET DATA" onPress={getData} />
      {dataUser.avatar.length > 0 && (
        <Image source={{uri: dataUser.avatar}} style={styles.avatar} />
      )}
      <Text>{`${dataUser.first_name} ${dataUser.last_name}`}</Text>
      <Text>{dataUser.email}</Text>
      <View style={styles.line} />
      <Button title="POST DATA" onPress={postData} />
      <Text>{dataJob.name}</Text>
      <Text>{dataJob.job}</Text>
    </View>
  );
};

export default CallAPIAxios;

const styles = StyleSheet.create({
  wrapper: {
    padding: 20,
  },
  title: {
    textAlign: 'center',
    marginBottom: 14,
  },
  line: {
    height: 2,
    marginVertical: 20,
    backgroundColor: '#383e56',
  },
  avatar: {
    height: 100,
    width: 100,
    borderRadius: 100,
  },
});
