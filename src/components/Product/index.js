import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import macbook from '../../assets/images/macabook.jpg';

const Product = ({onButtonPress}) => {
  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <Image source={macbook} style={styles.imageProduct} />
        <Text style={styles.titleProduct}>New Macbook Pro 2021</Text>
        <Text style={styles.priceProduct}>Rp. 25.000.000</Text>
        <Text style={styles.locationProduct}>Jakarta Barat</Text>
        <TouchableOpacity onPress={onButtonPress}>
          <View style={styles.buttonWrapper}>
            <Text style={styles.buttonText}>BELI</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Product;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  wrapper: {
    backgroundColor: '#f2f2f2',
    width: 212,
    borderRadius: 8,
    padding: 12,
  },
  imageProduct: {width: 188, height: 107, borderRadius: 8},
  titleProduct: {fontSize: 14, fontWeight: 'bold', marginTop: 16},
  priceProduct: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#f2994a',
    marginTop: 12,
  },
  locationProduct: {fontSize: 12, fontWeight: '300', marginTop: 12},
  buttonWrapper: {
    backgroundColor: '#6fcf97',
    paddingVertical: 6,
    borderRadius: 25,
    marginTop: 20,
  },
  buttonText: {
    fontSize: 14,
    fontWeight: '600',
    color: 'white',
    textAlign: 'center',
  },
});
